# Covid Graph Presentation

By Alvin B, PRODYNA UK

View master branch deployment at [https://al-v-in_docs.gitlab.io/pres/covid-graph/](https://al-v-in_docs.gitlab.io/pres/covid-graph/)

Presentation content at [slides.md](slides.md)

Based on [reveal.js](https://revealjs.com/)

### Dev

```
npm install
npm start
```
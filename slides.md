# Covid Graph Project Overview

#### Alvin B // Prodyna UK // 2020-12-03

Notes:
- aim: overview of the whole project
- focusing on relevant technologies to Prodyna UK
- sorry for being text heavy


## Overview

1. Covid Graph Introduction
1. Neo4j
1. Covid Graph Components
1. Wrapping up

---

# 👋 Introduction


## Setting the scene

- One global pandemic
- Several relevant datasets:
  - biological entities: genes, diseases, proteins etc
  - academic papers
  - Covid case statistics
  - medical patents
  - clinical trials
  - and more...
- Very many targeted data sources, databases etc

Notes:
- domain: biochemistry, pharma research etc
- not individual cases


## Unassailable data

- Data volume far too massive for any one person
- Information is siloed
- Entities have many synonyms
- Connections:
  - take time and effort to find (manually lookups)
  - may not be widely known
  - may be completely unknown

Notes:
- e.g. Genes to a disease, paper to gene to paper
- 128,000 papers regarding Covid 19 in 6 months
- New connections between genes and diseases found constantly


## The requirements

- Bring all the data together
- Connect the dots
- Maximize accessibility


## The solution: Covid Graph

- Neo4j knowledge graph
- Multiple databases into one
- Connecting the data wherever possible
- Graph algorithms/analysis

<img src="images/covid-Schema_v1.1_no_logo.png" width="70%">

Notes:
- PageRank algo: what's the gene most mentioned in Covid papers


## The Team & Organisation

<img src="images/covid-graph-team.png" width="100%">

Notes:
- various fields
- volunteers
- international
- open source
- fundraising

---

# 🌐 Neo4j

<!-- <img src="images/neo4j-database-meta-image.png" width="100%"> -->


## Summary

- NoSQL native graph database
- ACID compliant
- Nodes, labels, properties, relationships
- Schemaless
- Cypher query language
- Graph algorithms and analytics

Notes:
- one of the 4 NoSQL types: key-pair, document, columnar, graph
- Dijkstra (dyke-stra)


<!-- ## CAP Theory -->

<img src="images/cap-theory.jpg" width="70%">

<!-- Source: https://www.slideshare.net/semLiveEnv/cassandra-for-mission-critical-data -->


## Use Cases

- Knowledge Graph
- Master data management
- Real time recommendations
- Semantic search
- Fraud detection
- Network and IT operations
- Anything that looks like a graph (i.e. a lot!)


## Whiteboard model

<img src="images/matrix_whiteboard_model1.png" width="70%">

<!-- Source: https://neo4j.com/developer/guide-data-modeling/ -->


## Graph DB model

<img src="images/matrix_whiteboard_model3.png" width="70%">

<!-- Source: https://neo4j.com/developer/guide-data-modeling/ -->


## Demo

<img src="images/covid-Schema_v1.1_no_logo.png" width="70%">

Notes:
- Movie dataset
  - `MATCH (p:Person {name: 'Tom Hanks'})-[:ACTED_IN|:DIRECTED]->(m:Movie) RETURN p.name, m.title`
  - `MATCH (gene:Person)-[:ACTED_IN]->(m:Movie)<-[:ACTED_IN]-(other:Person) WHERE gene.name= 'Gene Hackman' AND exists( (other)-[:DIRECTED]->(m) ) RETURN  gene, other, m`
  - `MATCH p = shortestPath((m1:Movie)-[*]-(m2:Movie)) WHERE m1.title = 'A Few Good Men' AND m2.title = 'The Matrix' RETURN  p`
- CovidGraph
  - `CALL db.schema.visualization()`
  - `MATCH (gene:Gene) RETURN gene LIMIT 50`
  - `MATCH path=(gene:Gene)-[:EXPRESSED]->(tissue:GtexDetailedTissue) RETURN path LIMIT 50`
  - `MATCH path=(gene:Gene)-[:EXPRESSED]->(tissue:GtexDetailedTissue) WHERE gene.name = "ACE2" RETURN path LIMIT 50`
  - `MATCH path=(t:GtexDetailedTissue{name:'Lung'})<-[:EXPRESSED]-(g:Gene)-[:MAPS]-(:Gene)-[:MAPS]-(gs:GeneSymbol) return path limit 10`

---

# ⚒️ Covid Graph Components


## Sub Projects

- Data loading: docker pipelines
- Dev database
- GraphQL API
- Documentation
- Front end applications
- Non profit organisation


## Project Management

- Steering Group
- GitHub: [github.com/covidgraph](https://github.com/covidgraph)
- For all sub projects (a lot to manage)

Notes:
- Notion for core team in non profit


## Data loading

- Docker based pipelines
- Container for each data set and one to rule them all
- Neo4j credentials simply given as env vars
- Implementation agnostic (Python currently)
- Rules: idempotent, data modelling, naming conventions

Notes:
- create empty DB
- `docker build -t data_jhu_population .`
- `docker run --env GC_NEO4J_URL=bolt://myhost:7687 --env GC_NEO4J_USER=neo4j --env GC_NEO4J_PASSWORD=password --env RUN_MODE=test data_jhu_population`
- see populated DB



## Dev Database

- CovidGraph Lite
- Seed file generation
- Edge cases

Notes:
- WIP


## GraphQL API

- Single API for DB (and more?)
- Schema descriptions
- neo4j-graphql-js
- vs plain REST API vs Swagger & OpenAPI, HTTP/2 & Vulcain

Notes:
- Demo


## Documentation

- Document:
  - project overview
  - database access, tooling etc
  - database schema
- Docusaurus
- Schema docs auto-generation from GraphQL
- Goal: accessability

Notes:
- ideation to implementation
- researched various options
- lowering the barrier to entry
- deployment


## Front End Applications

- Visual Graph Explorer (yWorks)
- Unnamed Structr app
- Patent Search (React and GraphQL via GRANDstack)

Notes:
- may have heard of X Stack
- Patent Search
  - Few iterations, angular & cypher etc
  - Proof of concept
  - Challenge: no user stories etc
  - Potential overlap with Structr


## Non Profit Organisation

- Planning
- Branding
- Organising and PM
- Fundraising

---

# 🎄 Wrapping up


## Challenges

- No downward pressure/direction from client/users
- Knowledge bottlenecks
- Volunteers

Notes:
- Knowledge bottlenecks: often found in many projects


## Future Work

- ML/AI: Natural Language Processing (NLP)
- More data sources
  - GraphQL data loading
- Generalising CovidGraph (-> HealthGraph?)
- Infrastructure

Notes:
- as well continuing all sub projects mentioned


## Conclusion

- Good team
- Many interesting and relevant topics
- Hopeful of helping the people who help us

Notes:
- learnt a lot, a lot more to learn


# Danke!
